= MongoDB
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat aggregation framework

Utilitzarem la col·lecció link:db/mongodb/books.json[books].

Per importar la base de dades segueix els passos següents:

1. Descarrega el fitxer. Importa'l amb:
+
----
$ mongoimport -d test -c books <ruta al fitxer books.json>
----

2. El sistema t'ha d'informar que s'han importat 500 documents.

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- La consulta demanada.
- Els resultats obtinguts (o part dels resultats si en retorna molts).
====

1. Comprova que no hi ha títols repetits a la col·lecció: mostra els títols que
apareixen més d'una vegada, i la quantitat de cops que apareixen.

2. Quin és el benefici total obtingut amb el llibre _ZOMBIES WITHOUT A LEADER_?
+
[TIP]
====
Per accedir al preu de les ventes utilitza `"$sales.sale_price"`.
====

3. Cerca l'autor que ha escrit més llibres i quants llibres ha escrit. Si hi ha
diversos autors que han escrit el mateix nombre de llibres, agafa el primer pel
nom.

4. Cerca l'autor que ha escrit més llibres i quants llibres ha escrit. Si hi ha
diversos autors que han escrit el mateix nombre de llibres, mostra'ls tots.
+
[TIP]
====
Utilitza dues consultes.
====

5. Cerca el llibre que té una puntuació mitjana més alta. Mostra l'ISBN del
llibre, el títol, les seves puntuacions, i la mitjana de les seves puntuacions.

6. Cerca el llibre que té una puntuació mitjana més alta d'entre els que tenen
almenys dues puntuacions. Mostra l'ISBN del llibre, el títol, les seves
puntuacions, i la mitjana de les seves puntuacions.

7. Cerca el llibre de ciència ficció (_Science fiction_) que té una puntuació
mitjana més alta. Mostra'n el títol, l'ISBN, i la seva puntuació mitjana.

8. Per cada mes que apareix a la base de dades (des de maig de 2017 fins a maig
de 2018), cerca quants llibres s'han venut.
+
[TIP]
====
Utilitza les funcions `$month` i `$year`.
====

9. Per cada mes des de setembre de 2017 fins a febrer de 2018 (inclosos), cerca
el benefici total que hem obtingut.
+
[TIP]
====
Utilitza `new Date()` per crear una data.
====

10. Cerca el nom dels tres gèneres dels quals s'han escrit més llibres.

11. Cerca el nom dels gèneres dels quals s'han escrit més de 25 llibres.
Ordena'ls alfabèticament.

12. Calcula quants diners hem deixat de guanyar degut als descomptes aplicats.
+
[TIP]
====
Utilitza `$subtract`.
====

13. Cerca el títol del llibre que ens ha reportat més beneficis.

14. Cerca l'ISBN i el títol dels llibres venuts entre abril i maig de 2018, i el
benefici obtingut per cada títol en aquest període. Mostra només aquells pels
quals hem obtingut un benefici superior a 50 euros i ordena'ls pel benefici
obtingut.
+
[TIP]
====
Utilitza `new Date()` per crear una data.
====

15. Cerca l'autor que ens ha reportat més beneficis, i quants beneficis ens ha
reportat.

16. Cerca el número total de ventes que s'han fet de llibres que no tenen cap
puntuació, i els beneficis totals que hi hem obtingut.

17. Quins gèneres ha treballat la Miranda Belbin? Volem obtenir un array amb
tots aquests els gèneres.

18. Quins gèneres ha treballat l'autor del llibre amb ISBN 170494833-9?
+
[TIP]
====
Es pot fer en dues consultes, o en una de sola si utilitzes l'estadi `$lookup`.
====

19. Volem obtenir una llista amb tots els autors ordenada alfabèticament i,
per cada autor, l'ISBN dels llibres que ha escrit.

20. Volem comprovar que els preus amb descompte estan ben calculats a la
col·lecció. Fes una consulta que retorni els ISBN i les dades de les ventes que
estan mal calculades (hauria de donar 0 resultats amb les dades originals).
+
[TIP]
====
Per cada venta, calcula de nou el preu a partir del preu del llibre i del
descompte aplicat. Després, comprova si la diferència entre el preu que has
calculat i el preu de venta és superior a 0.01 (per evitar els efectes de
l'arrodoniment a 2 decimals).
====
