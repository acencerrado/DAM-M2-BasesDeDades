= El cinèfil
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

<<<

== Plantejament

Un aficionat al cinema ha decidit recopilar un munt d'informació sobre la
seva afició en una base de dades. Vol guardar informació sobre pel·lícules,
actors, directors, productores (estudis cinematogràfics), gèneres, etc.

Un cop muntada la base de dades voldrà respondre a preguntes de l'estil:

- En quines pel·lícules ha actuat Marlon Brando?
- Quins westerns ha protagonitzat Clint Eastwood?
- En quins tipus de pel·lícules ha actuat Gary Cooper?
- Amb quines productores ha treballat Santiago Segura (com a actor)?
- Quines pel·lícules va produir la Paramount durant el 2005?
- Quines pel·lícules va dirigir Woody Allen entre el 1995 i el 2005?
- Han actuat conjuntament en alguna pel·lícula la Sharon Stone i en
Silvester Stallone?
- Quin any es va estrenar la pel·lícula "Titanic"?
- En quines pel·lícules de El Deseo (productora) surt Carmen Maura?

== Qüestions

1. Identifica les entitats, atributs i relacions que són necessàries per
resoldre aquest problema.
2. Realitza un diagrama ER que permeti donar resposta a les preguntes
plantejades.
