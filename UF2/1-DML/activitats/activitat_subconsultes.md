# Activitat: Subconsultes

## Base de dades

Per aquests exercicis utilitzarem la base de dades _Chinook_.

Totes les qüestions s'han de resoldre utilitzant una única consulta.

## Consultes

1. Cerca quantes cançons hi ha que costin tant com la que costa més.

2. De les cançons de preu màxim, mostra les cinc últimes alfabèticament.

3. Mostra el nom i durada (en minuts) de les cançons que duren més de tres
vegades la durada mitjana de totes les cançons. Mostra el nom i la durada de
cadascuna de les cançons, en minuts, i ordena-les pel seu nom.

4. Cerca quines cançons tenen una durada superior a la que té la cançó
_Take the Celestra_. De cada cançó mostra'n el seu nom i la durada, en format
hora:minuts:segons (utilitza la funció *TIME_FORMAT*). Ordena els resultats
pel nom.

5. Cerca els tres grups/artistes dels quals hi ha més àlbums. Mostra només el
seu nom, ordenat alfabèticament.

6. El preu total d'una factura es pot trobar de dues maneres a la base de
dades: utilitzant la taula _InvoiceLine_, o mirant directament el _Total_ a la
taula _Invoice_. Fes una consulta que detecti qualsevol incongruència entre
aquestes dues xifres, és a dir, que mostri les factures per les quals no
coincideix aquest càlcul. Mostra l'id i el total de cada factura, i ordena-les
pel seu id.

7. Cerca quins àlbums tenen tantes cançons com l'àlbum anomenat
_Live After Death_. Mostra el seu títol i la quantitat de cançons que tenen, i
ordena els resultats pel títol.

8. Cerca quins àlbums tenen menys cançons. Mostra'n el títol i la quantitat de
cançons, i ordena els resultat pel títol.

9. Cerca l'id, el nom i el cognom dels clients que han comprat tantes cançons
com la clienta anomenada Camille Bernard. Mostra també la quantitat de cançons
comprada, i ordena els resultats per l'id dels clients.

10. Cerca quins àlbums tenen una durada inferior a l'àlbum anomenat
_Bach: Goldberg Variations_. Mostra el títol i la durada en minuts, i ordena
els resultats pel títol.

11. Cerca els àlbums la durada dels quals té una diferència de com a màxim un
10% amb la durada de l'àlbum _Bach: Golberg Variations_. És a dir, si aquest
àlbum dura, per exemple, 200 minuts, agafaríem tots els àlbums la durada dels
quals estigui entre 180 i 220 minuts. Mostra el títol i la durada en minuts, i
ordena els resultats pel títol.

12. Cerca els clients que han fet menys comandes que tots els clients de
Noruega (Norway). Mostra l'id, el nom i el cognom dels clients, i la quantitat
de comandes que han fet. Ordena els resultats per l'id dels clients.

13. Cerca els clients que han gastat menys que tots els clients de Noruega
(Norway). Mostra l'id, el nom i el cognom dels clients, i la quantitat total
que han gastat. Ordena els resultats per l'id dels clients.

14. Cerca els clients que han gastat un 20% més que almenys un dels clients de
Noruega (Norway). Mostra l'id, el nom i el cognom dels clients, i la quantitat
total que han gastat. Ordena els resultats per l'id dels clients.

15. Troba quins països tenen exactament la mateixa quantitat de factures
(_Invoice_) que el país que en té menys. Mostra el nom del país i la quantitat
de factures que té, i ordena els resultats pel nom del país.

16. Cerca els clients als quals se'ls ha facturat alguna vegada a una adreça
de França. Utilitza _EXISTS_. Mostra l'id, el nom i el cognom dels clients, i
ordena els resultats per l'id dels clients.

17. Cerca els clients que mai  han comprat una cançó composta per en Johann
Sebastian Bach. Utilitza _NOT EXISTS_. Mostra l'id, el nom i el cognom dels
clients, i ordena els resultats per l'id dels clients.
