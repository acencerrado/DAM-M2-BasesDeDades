# Activitat: Consultes d'agregació

## Base de dades

Per aquests exercicis utilitzarem la base de dades _Chinook_. Aquesta base de
dades està disponible a https://chinookdatabase.codeplex.com/ i es pot utilitzar
en diversos SGBD.

La base de dades _Chinook_ modelitza una botiga de música digital. Inclou
taules per àlbums, artistes, pistes (_Tracks_), clients, empleats i factures.

Per importar-la al MySQL/MariaDB cal descarregar el fitxer
_ChinookDatabase1.4_MySql.zip_, descomprimir-lo, i importar el fitxer
_Chinook_MySql.sql_.

## Consultes

1. Consulta quants àlbums hi ha a la base de dades.
2. Cerca quants grups (_Artist_) hi ha el nom dels quals comenci per l'article
_The_.
3. Amb dues consultes, cerca quants àlbums hi ha de Deep Purple.
4. Amb dues consultes, cerca els tres grups dels quals hi ha més àlbums.
5. Cerca la durada mitjana de totes les pistes. Volem el resultat en segons.
6. Amb tres consultes, cerca els 5 àlbums que tenen una durada mitjana de les
seves pistes més gran. De cada àlbum en volem el seu nom i el nom del grup.
7. Amb dues consultes, cerca el nom dels tres gèneres dels quals en tenim més
pistes.
8. Cerca quantes pistes hi ha en què hi consti algun compositor.
9. Cerca quants minuts de música disposem del compositor Johann Sebastian Bach.
10. Cerca el preu mitjà per pista. Després, cerca quantes pistes hi ha de
cadascun dels preus.
11. Selecciona els MB que ocupa cadascun dels àlbums. Ordena els resultats
de més a menys MB.
12. Amb dues consultes, obté el nom de les llistes (_playlist_) que contenen
més de mil cançons.
13. Descobreix quants clients tenen alguna dada introduïda a _State_. Fes-ho de
dues maneres diferents.
14. Descobreix quants clients tenim de cadascuna de les ciutats de Brasil.
15. Descobreix de quines ciutats d'Estats Units i de França tenim més d'un
client. Volem saber-ne el nom, si són d'un país o de l'altre, i quants clients
hi ha.
16. Amb dues consultes, troba quins països tenen exactament la mateixa
quantitat de factures (_invoice_) que el país que en té menys.
17. Utilitzant només la taula _InvoiceLine_, calcula el preu total de la
factura número 4. Després, comprova que la informació coincideix amb la que
hi ha a la taula _Invoice_.
18. Utilitzant només la taula _InvoiceLine_, cerca quines factures tenen un
preu total superior o igual a 20.
19. Cerca els identificadors de les cançons que ens han reportat més de 2 dólars
de beneficis.
20. Cerca des de quins països es fan comandes que, de mitjana, superen els 6
dólars de preu.
21. Sabent que la funció YEAR ens permet seleccionar l'any d'una data, troba
quants diners hem cobrat cada any. Ordena els resultats de més guanys a menys.
22. Sabent que la funció MONTH ens permet seleccionar el mes d'una data,
utilitza dues consultes per trobar per quins mesos la mitjana del preu de les
factures supera la mitjana global. Ordena els resultats de mitjanes més gran a
mitjanes més petites.
